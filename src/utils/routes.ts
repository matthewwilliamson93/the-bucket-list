import { Site } from "./sites";

export type Route = Array<Site>;

const DEGREES_TO_RADIANS = Math.PI / 180.0;
const RADIUS = 6378.388;

/*
 * A function to get the distance between two sites as the "crow flys".
 *
 * This can act as a pretty good substitute for the majority of cases
 * at the site level rather then using real-time routing information.
 *
 * Args:
 *    lat1: The first coordinates latititude.
 *    long1: The first coordinates longititude.
 *    lat2: The second coordinates latititude.
 *    long2: The second coordinates longititude.
 *
 * Returns:
 *    The distance between these sites.
 */
function distanceOnEarth(lat1: number, long1: number, lat2: number, long2: number): number {
    const phi1 = (90.0 - lat1) * DEGREES_TO_RADIANS;
    const phi2 = (90.0 - lat2) * DEGREES_TO_RADIANS;

    const theta1 = long1 * DEGREES_TO_RADIANS;
    const theta2 = long2 * DEGREES_TO_RADIANS;

    const cos =
        Math.sin(phi1) * Math.sin(phi2) * Math.cos(theta1 - theta2) +
        Math.cos(phi1) * Math.cos(phi2);
    const arc = Math.acos(cos);

    return arc * RADIUS;
}

/*
 * Gets the distance between two locations.
 *
 * Args:
 *    site1: The first location.
 *    site2: The second location.
 *
 * Returns:
 *    The distance between these locations.
 *
 */
function getDistanceBetweenSites(site1: Site, site2: Site): number {
    const pair = [site1.name, site2.name];
    pair.sort();

    const key = `distance-matrix-${pair[0]}-${pair[1]}`;
    const storedValue = window.localStorage.getItem(key);
    if (storedValue !== null) {
        return parseInt(storedValue);
    }

    const distance = distanceOnEarth(site1.lat, site1.long, site2.lat, site2.long);
    window.localStorage.setItem(key, distance.toString());

    return distance;
}

/*
 * Implements the swap in the 2opt algo.
 *
 * Reverses a section of a list between the start and end points.
 * https://en.wikipedia.org/wiki/2-opt
 *
 * Args:
 *    route: The current route.
 *    start: The start index of the swap.
 *    end: The end index of the index to swap.
 *
 * Returns:
 *    The new route after the swap.
 */
function swap2opt(route: Route, start: number, end: number): Route {
    const section = route.slice(start, end + 1);
    section.reverse();
    return [...route.slice(0, start), ...section, ...route.slice(end + 1)];
}

/*
 * Calculates the distance for the possible route.
 *
 * Args:
 *    route: The current route.
 *
 * Returns:
 *    The distance of the current route.
 */
function routeDistance(route: Route): number {
    let distance = 0;
    let lastSite = route[route.length - 1];
    route.forEach((currentSite) => {
        distance += getDistanceBetweenSites(currentSite, lastSite);
        lastSite = currentSite;
    });
    return distance;
}

/*
 * Uses a modified traveling salesman with 2-opt-swap to calculate
 * a possible route.
 *
 * Args:
 *    route: The current route.
 *
 * Returns:
 *    The new optimized route plan.
 */
export function planRoute(route: Route): Route {
    if (route.length < 3) {
        return route;
    }

    let improvement = true;
    let bestRoute = route;
    let bestDistance = routeDistance(route);
    while (improvement) {
        improvement = false;
        for (let i = 0; i < bestRoute.length - 1; i++) {
            for (let j = i + 1; j < bestRoute.length; j++) {
                const newRoute = swap2opt(bestRoute, i, j);
                const newDistance = routeDistance(newRoute);
                if (newDistance < bestDistance) {
                    bestRoute = newRoute;
                    bestDistance = newDistance;
                    improvement = true;
                    break;
                }
            }
            if (improvement) {
                break;
            }
        }
    }
    return bestRoute;
}
