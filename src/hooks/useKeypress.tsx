import { useEffect } from "react";

/*
 * A react hook that installs an action when a key is pressed.
 *
 * Args:
 *     key: The name of the key to respond to.
 *     action: The action to perform on key press.
 */
export function useKeypress(key: string, action: Function): void {
    useEffect(() => {
        const onKeyup = (e: KeyboardEvent) => {
            if (e.key !== key) {
                return;
            }

            action(e);
        };
        window.addEventListener("keyup", onKeyup);
        return () => window.removeEventListener("keyup", onKeyup);
    }, []);
}
