import { useCallback, useEffect, useState } from "react";

/*
 * A react hook that whose value is set by an Async function.
 *
 * This hook is triggered when the dependencies change, and it
 * calls the callback with the new value to load the new state.
 *
 * Args:
 *     callback: The callback function the hook uses to get the final state.
 *     dependencies: A list of values that the hook us triggered on.
 *
 * Returns:
 *     The react hooks loading state, error state, and value.
 */
export function useAsync<T>(
    callback: () => Promise<T>,
    dependencies: Array<unknown> = [],
): {
    loading: boolean;
    error: string | undefined;
    value: T | undefined;
} {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState<string>();
    const [value, setValue] = useState<T>();

    const callbackMemoized = useCallback(() => {
        setLoading(true);
        setError(undefined);
        setValue(undefined);
        callback()
            .then(setValue)
            .catch(setError)
            .finally(() => setLoading(false));
    }, dependencies);

    useEffect(() => {
        callbackMemoized();
    }, [callbackMemoized]);

    return { loading, error, value };
}
