import { useEffect, useState } from "react";

/*
 * A react hook whose value is set only after a timeout which
 * is reset if inputs change.
 *
 * This hook is triggered when the value changes, and it triggers a
 * timeout that will set value to load the new state.
 *
 * Often when you have someone typing or actively changing a value,
 * you can use this to not act on an input immediately.
 *
 * Args:
 *     value: The value to be set after the timer.
 *     delay: The amount of time to wait.
 *
 * Returns:
 *     The react hooks RO state to be read.
 */
export function useDebounce<T>(value: T, delay?: number): T {
    const [debouncedValue, setDebouncedValue] = useState<T>(value);

    useEffect(() => {
        const timer = setTimeout(() => setDebouncedValue(value), delay || 500);

        return () => {
            clearTimeout(timer);
        };
    }, [value, delay]);

    return debouncedValue;
}
