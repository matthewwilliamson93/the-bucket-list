import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

import { createStyles, makeStyles } from "@mui/styles";

import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CheckBoxOutlineBlankIcon from "@mui/icons-material/CheckBoxOutlineBlank";
import FlightIcon from "@mui/icons-material/Flight";
import DeleteIcon from "@mui/icons-material/Delete";
import LockIcon from "@mui/icons-material/Lock";
import LockOpenIcon from "@mui/icons-material/LockOpen";

import { useMotionValue, Reorder } from "framer-motion";

import React, { useState, Dispatch, SetStateAction } from "react";

import { useWithShadow } from "../hooks/useWithShadow";
import { Route } from "../utils/routes";
import { Site } from "../utils/sites";

const useStyles = makeStyles(() =>
    createStyles({
        travelList: {
            height: "auto",
            display: "grid",
            position: "relative",
            "list-style": "none",
        },
        travelItem: {
            "border-radius": "10px",
            "margin-bottom": "10px",
            position: "relative",
            display: "flex",
            "justify-content": "space-between",
            "align-items": "center",
            "flex-shrink": 0,
            cursor: "grab",
        },
    }),
);

/*
 * A React component that is meant to show a single card.
 *
 * https://mui.com/components/
 */
function TravelItem({
    site,
    index,
    isHome,
    setTrip,
    route,
    setRoute,
}: {
    site: Site;
    index: number;
    isHome: boolean;
    setTrip: Dispatch<SetStateAction<string | null>>;
    route: Route;
    setRoute: Dispatch<SetStateAction<Route>>;
}): JSX.Element {
    const classes = useStyles();

    const toggleBool = (index: number): void => {
        const updatedSite = route[index];
        updatedSite[isHome ? "haveBeen" : "islocked"] =
            !updatedSite[isHome ? "haveBeen" : "islocked"];
        setRoute([...route.slice(0, index), updatedSite, ...route.slice(index + 1)]);
    };

    const clickPlan = (index: number): void => {
        const trip = route[index].name;
        setTrip(trip);
        setRoute([route[index]]);
    };

    const clickTrash = (index: number): void => {
        setRoute([...route.slice(0, index), ...route.slice(index + 1)]);
    };

    const y = useMotionValue(0);
    const boxShadow = useWithShadow(y);

    return (
        <Reorder.Item
            className={classes.travelItem}
            key={`site-${index}`}
            value={site}
            style={{ boxShadow, y }}
        >
            <Card>
                <CardContent>
                    <Typography gutterBottom variant="body1" component="h2">
                        {site.inbetween
                            ? `${site.name} - ${site.inbetween} - ${site.country}`
                            : `${site.name} - ${site.country}`}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button
                        color="primary"
                        onClick={() => toggleBool(index)}
                        startIcon={
                            isHome ? (
                                site.haveBeen ? (
                                    <CheckBoxIcon />
                                ) : (
                                    <CheckBoxOutlineBlankIcon />
                                )
                            ) : site.islocked ? (
                                <LockIcon />
                            ) : (
                                <LockOpenIcon />
                            )
                        }
                        variant="contained"
                    >
                        {isHome ? "Visited?" : site.islocked ? "Free" : "Freeze"}
                    </Button>
                    {isHome ? (
                        <Button
                            color="success"
                            onClick={() => clickPlan(index)}
                            startIcon={<FlightIcon />}
                            variant="contained"
                        >
                            Plan
                        </Button>
                    ) : null}
                    <Button
                        color="error"
                        disabled={!isHome && site.islocked}
                        onClick={() => clickTrash(index)}
                        startIcon={<DeleteIcon />}
                        variant="contained"
                    >
                        Delete
                    </Button>
                </CardActions>
            </Card>
        </Reorder.Item>
    );
}

/*
 * A React component that is meant to show the list of sites.
 *
 * All of the sites can be drag-and-dropped, and each is meant to
 * display a card of actions for that site.
 *
 * https://mui.com/components/
 * https://mui.com/styles/api/
 * https://www.framer.com/motion/reorder/
 */
export default function TravelList({
    isHome,
    setTrip,
    route,
    setRoute,
}: {
    isHome: boolean;
    setTrip: Dispatch<SetStateAction<string | null>>;
    route: Route;
    setRoute: Dispatch<SetStateAction<Route>>;
}): JSX.Element {
    const classes = useStyles();

    return (
        <Box sx={{ maxHeight: "80vh", overflow: "auto", gap: "10px" }}>
            <Reorder.Group
                className={classes.travelList}
                axis="y"
                values={route}
                onReorder={setRoute}
                layoutScroll
                style={{ overflowY: "scroll" }}
            >
                {route.map((site: Site, index: number) => (
                    <TravelItem
                        key={`site-${index}`}
                        site={site}
                        index={index}
                        isHome={isHome}
                        setTrip={setTrip}
                        route={route}
                        setRoute={setRoute}
                    />
                ))}
            </Reorder.Group>
        </Box>
    );
}
