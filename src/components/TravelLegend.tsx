import { createStyles, makeStyles } from "@mui/styles";

import { scaleOrdinal } from "@visx/scale";
import { LegendItem, LegendLabel, LegendOrdinal } from "@visx/legend";

import React from "react";

import TravelPin from "./TravelPin";

const pinColorScale = scaleOrdinal({
    domain: ["Visited", "Planning To", "Never Been"],
    range: ["#4899f1", "#6ce18b", "#eb4d70"],
});

const useStyles = makeStyles(() =>
    createStyles({
        legend: {
            border: "1px solid rgba(255, 255, 255, 0.3)",
            "border-radius": "8px",
            color: "#efefef",
            "font-size": "10px",
            "font-family": "arial",
            "line-height": "0.9em",
            margin: "5px 5px",
            padding: "10px 10px",
        },
        title: {
            "font-size": "12px",
            "font-weight": "bold",
            "margin-bottom": "10px",
        },
        legends: {
            "background-color": "black",
            "border-radius": "14px",
            "flex-grow": 1,
            "font-family": "arial",
            "font-weight": 900,
            padding: "2px 2px 2px 2px",
            position: "relative",
            "overflow-y": "auto",
        },
    }),
);

/*
 * A React component that is meant to act as a legend for the pins.
 *
 * https://airbnb.io/visx/docs/legend
 * https://mui.com/styles/api/
 */
export default function TravelLegend(): JSX.Element {
    const classes = useStyles();

    return (
        <div className={classes.legends}>
            <div className={classes.legend}>
                <div className={classes.title}>Legend</div>
                <LegendOrdinal scale={pinColorScale}>
                    {(labels) =>
                        labels.map((label, i) => (
                            <LegendItem key={`legend-quantile-${i}`} margin="0 5px">
                                <TravelPin color={label.value as string} />
                                <LegendLabel align="left" margin="0 4px">
                                    {label.text}
                                </LegendLabel>
                            </LegendItem>
                        ))
                    }
                </LegendOrdinal>
            </div>
        </div>
    );
}
